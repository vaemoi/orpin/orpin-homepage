module.exports = {
  policy: [
    {
      userAgent: `Googlebot`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    },
    {
      userAgent: `Twitterbot`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    },
    {
      userAgent: `*`,
      allow: `/`,
      disallow: `/js`,
      crawlDelay: 2
    }
  ],
  sitemap: `https://orpin.vaemoi.xyz/sitemap.xml`,
  host: `https://orpin.vaemoi.xyz`
};
