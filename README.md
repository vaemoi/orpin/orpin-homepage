# orpin.vaemoi.xyz

orpin helps to manage npm packages in your web (read: browser) projects by keeping the `<script>` tags within your `<head></head>` element in sync with the versions in your package.json file.
