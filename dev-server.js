const bsConfig = {
  files: [
    `${__dirname}/public/css/main.css`,
    `${__dirname}/public/css/typography.css`
  ],
  open: false,
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`public/*.html`]
    }
  }],
  server: {
    baseDir: `public`,
    serveStaticOptions: {
      extensions: [`html`]
    }
  }
};

require(`browser-sync`).init(bsConfig);
