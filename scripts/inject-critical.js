const
  fs = require(`fs`),
  cheerio = require(`cheerio`),
  path = require(`path`);

const htmlFiles = [
  `../public/index.html`,
  `../public/about.html`
];

let criticalCSS = fs.readFileSync(
  path.join(__dirname, `../public/css/critical.css`)
);

if (Buffer.isBuffer(criticalCSS)) {
  criticalCSS = criticalCSS.toString();
}

for (const htmlFile of htmlFiles) {
  const
    filePath = path.join(__dirname, htmlFile),
    fileDOM = cheerio.load(fs.readFileSync(filePath));

  fileDOM(`<style>${criticalCSS}</style>`).insertAfter(`title`);

  fs.writeFileSync(filePath, fileDOM.html());
}
