const
  fs = require(`fs`),
  cheerio = require(`cheerio`),
  path = require(`path`);

const
  htmlFiles = [`../public/index.html`, `../public/about.html`],
  svgSpriteSheet = fs.readFileSync(
    path.resolve(__dirname, `../assets/svg/spritesheet.svg`),
    {encoding: `utf8`}
  );

htmlFiles.forEach((file) => {
  const
    htmlPath = path.resolve(__dirname, file),
    html = cheerio.load(fs.readFileSync(htmlPath, {encoding: `utf8`}));

  html(svgSpriteSheet).insertBefore(`header`);
  fs.writeFileSync(htmlPath, html.html());
});
